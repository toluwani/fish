<!Doctype html>
<html class="no-js" lang="en">
    

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Fish Scheme</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    
    
        <!-- Favicon -->
        <link rel="shortcut icon" href="../favicon.ico">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/styles/vendor/bootstrap.min.css">
        <!-- Fonts -->
        <link rel="stylesheet" href="assets/fonts/et-lineicons/css/style.css">
        <link rel="stylesheet" href="assets/fonts/linea-font/css/linea-font.css">
        <link rel="stylesheet" href="assets/fonts/fontawesome/css/font-awesome.min.css">
        <!-- Slider -->
        <link rel="stylesheet" href="assets/styles/vendor/slick.css">
        <!-- Lightbox -->
        <link rel="stylesheet" href="assets/styles/vendor/magnific-popup.css">
        <!-- Animate.css -->
        <link rel="stylesheet" href="assets/styles/vendor/animate.css">


        <!-- Definity CSS -->
        <link rel="stylesheet" href="assets/styles/main.css">
        <link rel="stylesheet" href="assets/styles/responsive.css">

        <!-- JS -->
        <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body id="page-top">
        <!-- Google Tag Manager -->
        <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-MH7TSF"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MH7TSF');</script>
        <!-- End Google Tag Manager -->
        
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <!-- ========== Preloader ========== -->

       
        

    <header>
    <div class="welcome_header">
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <h6>Affortable housing  synergy</h6>
            </div>
            <div class="col-sm-6 text-right">
              <ul class="welcome_header_menu">                
                <li class="facebook_icon"><a href="#"><i class="fa fa-facebook"> </i></a> </li>
                <li class="twitter_icon"><a href="#"><i class="fa fa-twitter"> </i></a> </li>
                <li class="google_plus_icon"><a href="#"><i class="fa fa-google-plus"> </i></a> </li>
                <li class="pinterest_icon"><a href="#"><i class="fa fa-pinterest"> </i></a> </li>
                <li class="youtube_icon"><a href="#"><i class="fa fa-youtube-play"> </i></a> </li>
              </ul>       
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="menu">
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"> </span>
                    <span class="icon-bar"> </span>
                    <span class="icon-bar"> </span>
                  </button>
                  <a class="navbar-brand" href="index.php">
                    <img src="images/logo.png" class="img-respnsive">
                  </a>
                </div>
            
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                  <ul class="nav navbar-right navbar-nav nav_link ">
                   <li><a href="index.php">HOME </a>
                    
                  </li>
                   
                                     
                    <li><a href="gallery.php">GALLERY </a>
                    
                    </li>
                    
                <li><a href="register.php">REGISTER </a> </li>
                
                    
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
        </div>
        
      </div>
    </header/>
        <!-- ========== Login From ========== -->

        <div class="login-1">
          <div class="bg-overlay">
            
            <div class="container">
              <div class="row">
                <div class=" col-md-12">
                  
                  <!-- Log in -->
                  <div class="panel-group" id="login-accordion" role="tablist" aria-multiselectable="true">
                       <div class="panel top-panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#login-accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            The Federal integrated staff housing
                            Application for property allocation
                          </a>
                        </h4>
                      </div>
                       <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">

                          <div class="form-login">
                            <form action="login2.inc.php" method="POST">
                              <!-- First Name -->
                               
                              <div class="form-group col-md-6 ">
                                <input type="text" id="name-login-1" class="form-control" placeholder="Full Name" name="fullname" required>
                                <label for="name-login-1">Full name of applicant</label>
                              </div>                            

                              <div class="form-group col-md-6">
                                <input type="date" class="form-control" placeholder="" name="dob" required>
                                <label for="pass-signup-1">Date of birth</label> 
                              </div>
                              <!-- Password -->
                              <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder=" enter your state" name="state" required>
                                <label for="pass-signup-1">State of origin</label>
                              </div>
                              <!-- Re-Enter Password -->
                              <div class="form-group col-md-6">
                               <select class="form-control" name="nationality">
  <option value="AF">Afghanistan</option>
  <option value="AX">Åland Islands</option>
  <option value="AL">Albania</option>
  <option value="DZ">Algeria</option>
  <option value="AS">American Samoa</option>
  <option value="AD">Andorra</option>
  <option value="AO">Angola</option>
  <option value="AI">Anguilla</option>
  <option value="AQ">Antarctica</option>
  <option value="AG">Antigua and Barbuda</option>
  <option value="AR">Argentina</option>
  <option value="AM">Armenia</option>
  <option value="AW">Aruba</option>
  <option value="AU">Australia</option>
  <option value="AT">Austria</option>
  <option value="AZ">Azerbaijan</option>
  <option value="BS">Bahamas</option>
  <option value="BH">Bahrain</option>
  <option value="BD">Bangladesh</option>
  <option value="BB">Barbados</option>
  <option value="BY">Belarus</option>
  <option value="BE">Belgium</option>
  <option value="BZ">Belize</option>
  <option value="BJ">Benin</option>
  <option value="BM">Bermuda</option>
  <option value="BT">Bhutan</option>
  <option value="BO">Bolivia, Plurinational State of</option>
  <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
  <option value="BA">Bosnia and Herzegovina</option>
  <option value="BW">Botswana</option>
  <option value="BV">Bouvet Island</option>
  <option value="BR">Brazil</option>
  <option value="IO">British Indian Ocean Territory</option>
  <option value="BN">Brunei Darussalam</option>
  <option value="BG">Bulgaria</option>
  <option value="BF">Burkina Faso</option>
  <option value="BI">Burundi</option>
  <option value="KH">Cambodia</option>
  <option value="CM">Cameroon</option>
  <option value="CA">Canada</option>
  <option value="CV">Cape Verde</option>
  <option value="KY">Cayman Islands</option>
  <option value="CF">Central African Republic</option>
  <option value="TD">Chad</option>
  <option value="CL">Chile</option>
  <option value="CN">China</option>
  <option value="CX">Christmas Island</option>
  <option value="CC">Cocos (Keeling) Islands</option>
  <option value="CO">Colombia</option>
  <option value="KM">Comoros</option>
  <option value="CG">Congo</option>
  <option value="CD">Congo, the Democratic Republic of the</option>
  <option value="CK">Cook Islands</option>
  <option value="CR">Costa Rica</option>
  <option value="CI">Côte d'Ivoire</option>
  <option value="HR">Croatia</option>
  <option value="CU">Cuba</option>
  <option value="CW">Curaçao</option>
  <option value="CY">Cyprus</option>
  <option value="CZ">Czech Republic</option>
  <option value="DK">Denmark</option>
  <option value="DJ">Djibouti</option>
  <option value="DM">Dominica</option>
  <option value="DO">Dominican Republic</option>
  <option value="EC">Ecuador</option>
  <option value="EG">Egypt</option>
  <option value="SV">El Salvador</option>
  <option value="GQ">Equatorial Guinea</option>
  <option value="ER">Eritrea</option>
  <option value="EE">Estonia</option>
  <option value="ET">Ethiopia</option>
  <option value="FK">Falkland Islands (Malvinas)</option>
  <option value="FO">Faroe Islands</option>
  <option value="FJ">Fiji</option>
  <option value="FI">Finland</option>
  <option value="FR">France</option>
  <option value="GF">French Guiana</option>
  <option value="PF">French Polynesia</option>
  <option value="TF">French Southern Territories</option>
  <option value="GA">Gabon</option>
  <option value="GM">Gambia</option>
  <option value="GE">Georgia</option>
  <option value="DE">Germany</option>
  <option value="GH">Ghana</option>
  <option value="GI">Gibraltar</option>
  <option value="GR">Greece</option>
  <option value="GL">Greenland</option>
  <option value="GD">Grenada</option>
  <option value="GP">Guadeloupe</option>
  <option value="GU">Guam</option>
  <option value="GT">Guatemala</option>
  <option value="GG">Guernsey</option>
  <option value="GN">Guinea</option>
  <option value="GW">Guinea-Bissau</option>
  <option value="GY">Guyana</option>
  <option value="HT">Haiti</option>
  <option value="HM">Heard Island and McDonald Islands</option>
  <option value="VA">Holy See (Vatican City State)</option>
  <option value="HN">Honduras</option>
  <option value="HK">Hong Kong</option>
  <option value="HU">Hungary</option>
  <option value="IS">Iceland</option>
  <option value="IN">India</option>
  <option value="ID">Indonesia</option>
  <option value="IR">Iran, Islamic Republic of</option>
  <option value="IQ">Iraq</option>
  <option value="IE">Ireland</option>
  <option value="IM">Isle of Man</option>
  <option value="IL">Israel</option>
  <option value="IT">Italy</option>
  <option value="JM">Jamaica</option>
  <option value="JP">Japan</option>
  <option value="JE">Jersey</option>
  <option value="JO">Jordan</option>
  <option value="KZ">Kazakhstan</option>
  <option value="KE">Kenya</option>
  <option value="KI">Kiribati</option>
  <option value="KP">Korea, Democratic People's Republic of</option>
  <option value="KR">Korea, Republic of</option>
  <option value="KW">Kuwait</option>
  <option value="KG">Kyrgyzstan</option>
  <option value="LA">Lao People's Democratic Republic</option>
  <option value="LV">Latvia</option>
  <option value="LB">Lebanon</option>
  <option value="LS">Lesotho</option>
  <option value="LR">Liberia</option>
  <option value="LY">Libya</option>
  <option value="LI">Liechtenstein</option>
  <option value="LT">Lithuania</option>
  <option value="LU">Luxembourg</option>
  <option value="MO">Macao</option>
  <option value="MK">Macedonia, the former Yugoslav Republic of</option>
  <option value="MG">Madagascar</option>
  <option value="MW">Malawi</option>
  <option value="MY">Malaysia</option>
  <option value="MV">Maldives</option>
  <option value="ML">Mali</option>
  <option value="MT">Malta</option>
  <option value="MH">Marshall Islands</option>
  <option value="MQ">Martinique</option>
  <option value="MR">Mauritania</option>
  <option value="MU">Mauritius</option>
  <option value="YT">Mayotte</option>
  <option value="MX">Mexico</option>
  <option value="FM">Micronesia, Federated States of</option>
  <option value="MD">Moldova, Republic of</option>
  <option value="MC">Monaco</option>
  <option value="MN">Mongolia</option>
  <option value="ME">Montenegro</option>
  <option value="MS">Montserrat</option>
  <option value="MA">Morocco</option>
  <option value="MZ">Mozambique</option>
  <option value="MM">Myanmar</option>
  <option value="NA">Namibia</option>
  <option value="NR">Nauru</option>
  <option value="NP">Nepal</option>
  <option value="NL">Netherlands</option>
  <option value="NC">New Caledonia</option>
  <option value="NZ">New Zealand</option>
  <option value="NI">Nicaragua</option>
  <option value="NE">Niger</option>
  <option value="NG"  >Nigeria</option>
  <option value="NU">Niue</option>
  <option value="NF">Norfolk Island</option>
  <option value="MP">Northern Mariana Islands</option>
  <option value="NO">Norway</option>
  <option value="OM">Oman</option>
  <option value="PK">Pakistan</option>
  <option value="PW">Palau</option>
  <option value="PS">Palestinian Territory, Occupied</option>
  <option value="PA">Panama</option>
  <option value="PG">Papua New Guinea</option>
  <option value="PY">Paraguay</option>
  <option value="PE">Peru</option>
  <option value="PH">Philippines</option>
  <option value="PN">Pitcairn</option>
  <option value="PL">Poland</option>
  <option value="PT">Portugal</option>
  <option value="PR">Puerto Rico</option>
  <option value="QA">Qatar</option>
  <option value="RE">Réunion</option>
  <option value="RO">Romania</option>
  <option value="RU">Russian Federation</option>
  <option value="RW">Rwanda</option>
  <option value="BL">Saint Barthélemy</option>
  <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
  <option value="KN">Saint Kitts and Nevis</option>
  <option value="LC">Saint Lucia</option>
  <option value="MF">Saint Martin (French part)</option>
  <option value="PM">Saint Pierre and Miquelon</option>
  <option value="VC">Saint Vincent and the Grenadines</option>
  <option value="WS">Samoa</option>
  <option value="SM">San Marino</option>
  <option value="ST">Sao Tome and Principe</option>
  <option value="SA">Saudi Arabia</option>
  <option value="SN">Senegal</option>
  <option value="RS">Serbia</option>
  <option value="SC">Seychelles</option>
  <option value="SL">Sierra Leone</option>
  <option value="SG">Singapore</option>
  <option value="SX">Sint Maarten (Dutch part)</option>
  <option value="SK">Slovakia</option>
  <option value="SI">Slovenia</option>
  <option value="SB">Solomon Islands</option>
  <option value="SO">Somalia</option>
  <option value="ZA">South Africa</option>
  <option value="GS">South Georgia and the South Sandwich Islands</option>
  <option value="SS">South Sudan</option>
  <option value="ES">Spain</option>
  <option value="LK">Sri Lanka</option>
  <option value="SD">Sudan</option>
  <option value="SR">Suriname</option>
  <option value="SJ">Svalbard and Jan Mayen</option>
  <option value="SZ">Swaziland</option>
  <option value="SE">Sweden</option>
  <option value="CH">Switzerland</option>
  <option value="SY">Syrian Arab Republic</option>
  <option value="TW">Taiwan, Province of China</option>
  <option value="TJ">Tajikistan</option>
  <option value="TZ">Tanzania, United Republic of</option>
  <option value="TH">Thailand</option>
  <option value="TL">Timor-Leste</option>
  <option value="TG">Togo</option>
  <option value="TK">Tokelau</option>
  <option value="TO">Tonga</option>
  <option value="TT">Trinidad and Tobago</option>
  <option value="TN">Tunisia</option>
  <option value="TR">Turkey</option>
  <option value="TM">Turkmenistan</option>
  <option value="TC">Turks and Caicos Islands</option>
  <option value="TV">Tuvalu</option>
  <option value="UG">Uganda</option>
  <option value="UA">Ukraine</option>
  <option value="AE">United Arab Emirates</option>
  <option value="GB">United Kingdom</option>
  <option value="US">United States</option>
  <option value="UM">United States Minor Outlying Islands</option>
  <option value="UY">Uruguay</option>
  <option value="UZ">Uzbekistan</option>
  <option value="VU">Vanuatu</option>
  <option value="VE">Venezuela, Bolivarian Republic of</option>
  <option value="VN">Viet Nam</option>
  <option value="VG">Virgin Islands, British</option>
  <option value="VI">Virgin Islands, U.S.</option>
  <option value="WF">Wallis and Futuna</option>
  <option value="EH">Western Sahara</option>
  <option value="YE">Yemen</option>
  <option value="ZM">Zambia</option>
  <option value="ZW">Zimbabwe</option>
</select>
                                <label for="re-pass-signup-1"> Nationality</label>

                                
                              </div>
                              <!-- Newsletter check -->
                               <div class="form-group col-md-6">
                                <input type="Email" id="re-pass-signup-1" class="form-control" placeholder="Email" name="email" required>
                                <label for="re-pass-signup-1"> Email</label>
                              </div>

                              <!-- Re-Enter Password -->
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Enter Tel-No" name="tel" required>
                                <label for="re-pass-signup-1"> Telphone no</label>
                              </div>
                              <!-- Re-Enter Password -->
                              <div class="form-group col-md-6 no-gap-left">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Enter Adress" name="address" required>
                                <label for="re-pass-signup-1"> Address</label>
                              </div>

                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Enter Adress" name="rank" required>
                                <label for="re-pass-signup-1"> rank</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="checkbox" id="re-pass-signup-1" class="form-control" placeholder="" value="male" name="gender"  >
                                <label for="re-pass-signup-1"> Male</label> 
                                <input type="checkbox" id="re-pass-signup-1" class="form-control" placeholder="Rank" name="gender" value="female">
                                <label for="re-pass-signup-1">Female</label> 
                              </div>
                               <div class="form-group col-md-6">
                                <input type="date" id="re-pass-signup-1" class="form-control" placeholder="Date of appointment" name="dateofappointment" required>
                                <label for="re-pass-signup-1"> Date of Appointment</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="date" id="re-pass-signup-1" class="form-control" placeholder="retirement date" name="expectedretirementdate" required>
                                <label for="re-pass-signup-1"> Expected date of retirement </label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Identification" name="identificationtype">
                                <label for="re-pass-signup-1"> Identification type</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Fish-no" name="fishno" required>
                                <label for="re-pass-signup-1"> Fish membership no/date</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Next of kin" name="nextofkin" required>
                                <label for="re-pass-signup-1"> Next of kin</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="relationship" name="relationshipofnextofkin" required>
                                <label for="re-pass-signup-1"> Relationship of next of kin</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder=" Describe the property" name="descriptionproperty" required>
                                <label for="re-pass-signup-1"> Description of property</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Developer" name="nameofdeveloper" required>
                                <label for="re-pass-signup-1"> Name of developer</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Location" name="preferredlocation" required>
                                <label for="re-pass-signup-1"> Preferred location</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="price" name="propertyprice" required>
                                <label for="re-pass-signup-1"> Preffered poperty price</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Equity contribution" name="equity" required>
                                <label for="re-pass-signup-1"> Equity contribution if Avaliable</label>
                              </div><div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control col-md-6" placeholder="Morgage amount"  name="morgageamountrequired" required>
                                <label for="re-pass-signup-1"> Morgage amount Requirment</label>
                              </div>

                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Morgage price" name="morgageprice" required>
                                <label for="re-pass-signup-1"> morgage affordability price</label>
                              </div>

                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Morgage profile" name="morgageprofile" required>
                                <label for="re-pass-signup-1"> morgage Affordability Profile</label>
                              </div>

                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder=" National housing number" name="nationalhousingnumber" required>
                                <label for="re-pass-signup-1"> National  Housing Fund  Number</label>
                              </div>


                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Evidence of loan application" name="evidenceofloan" required>
                                <label for="re-pass-signup-1"> Evidence of loan application</label>
                              </div>

                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" placeholder="Evidence of processing fee" name="evidenceofprocessfee" required>
                                <label for="re-pass-signup-1"> Evidence of processing fee</label>
                              </div>


                              <!-- Submit -->
                              <input type="submit" value="Register" class="btn">
                              <a href="login1.html">click here for next page </a>
                            </form>
                          </div><!-- / .form-wrapper -->

                        </div><!-- / .panel-body -->
                      </div><!-- / #register-login-page-2 .panel-colapse -->
                    </div><!-- / .panel -->
                    
                    <!-- Register Account -->
                 
                  </div><!-- / #login-accordion .panel-group -->


                </div><!-- / .col-md-6 -->
              </div><!-- / .row -->
            </div><!-- / .container -->

          </div><!-- / .bg-overlay -->
        </div><!-- / .bg-login -->



    

        <!-- ========== Scripts ========== -->

        <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="assets/js/vendor/google-fonts.js"></script>
        <script src="assets/js/vendor/jquery.easing.js"></script>
        <script src="assets/js/vendor/jquery.waypoints.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/vendor/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/vendor/smoothscroll.js"></script>
        <script src="assets/js/vendor/jquery.localScroll.min.js"></script>
        <script src="assets/js/vendor/jquery.scrollTo.min.js"></script>
        <script src="assets/js/vendor/jquery.stellar.min.js"></script>
        <script src="assets/js/vendor/jquery.parallax.js"></script>
        <script src="assets/js/vendor/slick.min.js"></script>
        <script src="assets/js/vendor/jquery.easypiechart.min.js"></script>
        <script src="assets/js/vendor/countup.min.js"></script>
        <script src="assets/js/vendor/isotope.min.js"></script>
        <script src="assets/js/vendor/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/vendor/wow.min.js"></script>

        <!-- Definity JS -->
        <script src="assets/js/main.js"></script>
    </body>

</html>
