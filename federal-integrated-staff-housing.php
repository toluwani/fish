<!doctype html>
<html class="no-js" lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Fish Scheme</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    
    
        <!-- Favicon -->
        <link rel="shortcut icon" href="../favicon.ico">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/styles/vendor/bootstrap.min.css">
        <!-- Fonts -->
        <link rel="stylesheet" href="assets/fonts/et-lineicons/css/style.css">
        <link rel="stylesheet" href="assets/fonts/linea-font/css/linea-font.css">
        <link rel="stylesheet" href="assets/fonts/fontawesome/css/font-awesome.min.css">
        <!-- Slider -->
        <link rel="stylesheet" href="assets/styles/vendor/slick.css">
        <!-- Lightbox -->
        <link rel="stylesheet" href="assets/styles/vendor/magnific-popup.css">
        <!-- Animate.css -->
        <link rel="stylesheet" href="assets/styles/vendor/animate.css">


        <!-- Definity CSS -->
        <link rel="stylesheet" href="assets/styles/main.css">
        <link rel="stylesheet" href="assets/styles/responsive.css">

        <!-- JS -->
        <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body id="page-top">
        <!-- Google Tag Manager -->
        <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-MH7TSF"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MH7TSF');</script>
        <!-- End Google Tag Manager -->
        
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <!-- ========== Preloader ========== -->

       
        

    <div class="welcome_header">
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <h6>Affortable housing synergy</h6>
            </div>
            <div class="col-sm-6 text-right">
              <ul class="welcome_header_menu">                
                <li class="facebook_icon"><a href="#"><i class="fa fa-facebook"> </i></a> </li>
                <li class="twitter_icon"><a href="#"><i class="fa fa-twitter"> </i></a> </li>
                <li class="google_plus_icon"><a href="#"><i class="fa fa-google-plus"> </i></a> </li>
                <li class="pinterest_icon"><a href="#"><i class="fa fa-pinterest"> </i></a> </li>
                <li class="youtube_icon"><a href="#"><i class="fa fa-youtube-play"> </i></a> </li>
              </ul>       
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="menu">
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"> </span>
                    <span class="icon-bar"> </span>
                    <span class="icon-bar"> </span>
                  </button>
                  <a class="navbar-brand" href="index.php">
                    <img src="images/logo.png" alt="logo">
                  </a>
                </div>
            
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-right navbar-nav nav_link ">
                    <li><a href="#">HOME </a>
                      
                  </li>
                   
                                
                    <li><a href="#">GALLERY </a>
                    </li>
                    
                <li><a href="register.php">Register </a> </li>
                
                    
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
        </div>
        
      </div>
        <!-- ========== Login From ========== -->

        <div class="login-1">
          <div class="bg-overlay">
            
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  
                  <!-- Log in -->
                  <div class="panel-group" id="login-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel top-panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                             The Federal integrated staff housing
                            Application for property allocation
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">

                         <div class="form-register">
                            <form >
                              <!-- First Name -->

                             
                              <div class="form-group col-md-6 ">
                                <input type="text" id="name-login-1" class="form-control" name="surname" placeholder="Surname">
                                <label for="name-login-1">Surname</label>
                              </div>
                              <!-- Last name -->
                             
                              <!-- Password -->
                              <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" name="middlename" placeholder="MiddleName" >
                                <label for="pass-signup-1">Middle Name</label>
                              </div>
                              <!-- Re-Enter Password -->
                              <div class="form-group col-md-6">
                                <input type="text" id="re-pass-signup-1" class="form-control" name="lastname" placeholder="LastName" >
                                <label for="re-pass-signup-1">Last Name</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="checkbox" id="lastname-signup-1" class="form-control" name="gender" value="M">
                                <label for="last-name-signup-1">Male</label>
                              <input type="checkbox" id="email-signup-1" class="form-control" placeholder="F" name="gender" value="F">
                                <label for="email-signup-1">Female</label>
                              </div>
                              <div class="form-group col-md-6">
                              <select class="form-control" name="ministry">
                                <option value="Ministry">Ministry</option>
                                <option value="Parastatal">Parastatal</option>
                                <option value="Agency">Agency</option>
                          
                              </select>
                                <label for="pass-signup-1">Ministry/Parastatals/Agency</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="date" id="pass-signup-1" class="form-control" name="dob" placeholder="01/01/1950">
                                <label for="pass-signup-1">Date Of Birth</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="number" id="pass-signup-1" class="form-control" name="age" placeholder="35" required>
                                <label for="pass-signup-1">Age</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" name="nextofkin" placeholder="Paul Caleb" required>
                                <label for="pass-signup-1">Name of Next of Kin</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" name="relationship" placeholder="">
                                <label for="pass-signup-1">Relationship</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" name="addressnextofkin" placeholder="">
                                <label for="pass-signup-1">Address of Next of Kin</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" name="rank" placeholder="" required>
                                <label for="pass-signup-1">Rank</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" name="establishmentNo" placeholder="" required>
                                <label for="pass-signup-1">Estblishment No.</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="date" id="pass-signup-1" class="form-control" placeholder="" name="dateofappointment" required>
                                <label for="pass-signup-1">Date of First Appointment</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="date" id="pass-signup-1" name="presentappointmentdate" class="form-control" placeholder="" required>
                                <label for="pass-signup-1">Date of Present Appointment</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="salary" required>
                                <label for="pass-signup-1">Salary Grade Level & Step</label>
                              </div>
                               <div class="form-group col-md-6" required>
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="state" required >
                                <label for="pass-signup-1">State of Origin</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="localgovt"required >
                                <label for="pass-signup-1">Local Govt.</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="confirmedpensionableappointment" required >
                                <label for="pass-signup-1">Confirmed in pensionable appointment</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="gazette" required >
                                <label for="pass-signup-1">Gazette or other particulars</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="pensionfundadmin" required >
                                <label for="pass-signup-1">Pension Fund Administrator</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="pemcomNo" required >
                                <label for="pass-signup-1">PENCOM PIN No.</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="NHF" required >
                                <label for="pass-signup-1">National Housing Fund (NHF) No.</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="ippisNo" required >
                                <label for="pass-signup-1">IPPIS No.</label>
                              </div>
                               <div class="form-group col-md-8">
                               <label for="pass-signup-1">Types of Houses Required: </label></br>
                                <input type="checkbox" id="pass-signup-1" class="form-control" placeholder="" value="1-bedroom" name="typeofhouse">
                                <label for="pass-signup-1">1-bedroom</label>
                                <input type="checkbox" id="pass-signup-1" class="form-control" placeholder="" value="2-bedroom" name="typeofhouse">
                                <label for="pass-signup-1">2-bedroom</label>
                                <input type="checkbox" id="pass-signup-1" class="form-control" placeholder="" value="3-bedroom" name="typeofhouse">
                                <label for="pass-signup-1">3-bedroom</label>
                                <input type="checkbox" id="pass-signup-1" class="form-control" placeholder="" value="3-bedroom Duplex" name="typeofhouse">
                                <label for="pass-signup-1">3-bedroom Duplex</label>
                                 <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="typeofhouse">
                                <label for="pass-signup-1">Others specify.</label>
                              </div>
                               <div class="form-group col-md-6">
                                <label for="pass-signup-1">The location for the desired House: </label>
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="locationofhouse" value="state">
                                <label for="email-signup-1">State</label>
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="locationofhouse" value="LGA">
                                <label for="email-signup-1">LGA</label>
                              </div>
                               <div class="form-group col-md-6">
                                <label for="pass-signup-1">Have you taken Housing Loan before? </label>
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="takenloanbefore" value="fgshlb">
                                <label for="email-signup-1">FGSHLB</label>
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="takenloanbefore" value="cooperative society">
                                <label for="email-signup-1">Cooperative Society</label>
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="takenloanbefore" value="bank">
                                <label for="email-signup-1">Banks</label>
                                <input type="text" id="email-signup-1" class="form-control" placeholder="" name="takenloanbefore">
                                <label for="email-signup-1">Other specify.</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="pass-signup-1" class="form-control" placeholder="" name="amountrequiredmorgage" required >
                                <label for="pass-signup-1">Amount required from Mortage</label>
                              </div>
                               <div class="form-group col-md-6">
                                <input type="text" id="email-signup-1" class="form-control" placeholder="" name="numyearsinservice" required >
                                <label for="pass-signup-1">Number of years spent in Service: </label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="email-signup-1" class="form-control" placeholder="" name="remainingyears" >
                                <label for="email-signup-1">Remaining years</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="date" id="email-signup-1" class="form-control" placeholder="" name="retirementfromservice" >
                                <label for="email-signup-1">Retirement from Service Date</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="email-signup-1" class="form-control" placeholder="" name="repaymentyears" required >
                                <label for="email-signup-1">Number of years proposed for repayment</label>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="email-signup-1">Equity Contribution:</label>
                             
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="equitycontribution" value="10%" >
                                <label for="email-signup-1">10%</label>
                             
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="equitycontribution" value="20%">
                                <label for="email-signup-1">20%</label>
                            
                                <input type="checkbox" id="email-signup-1" class="form-control" placeholder="" name="equitycontribution" value="30%">
                                <label for="email-signup-1">30%</label>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" id="email-signup-1" class="form-control" placeholder="" name="monthlysalary" required>
                                <label for="email-signup-1">Monthly Salary</label>
                              </div>
                              <!-- Newsletter check -->
                              <div class="form-group col-md-12">
                                <label class="checkbox-inline">
                                  <input type="checkbox" id="newsletter-check" value="option1" checked required> I agree to terms and condition
                                </label>
                              </div>
                              <!-- Submit -->
                              <div class="col-md-6">
                              <input type="submit" value="Register" class="btn">
                              <a href="login.html">click here for next page </a>
                              </div>
                            </form>
                          </div><!-- / .form-wrapper -->

                        </div><!-- / .panel-body -->
                      </div><!-- / .panel-collapse -->
                    </div><!-- / .panel -->
                    
                    <!-- Register Account -->
                  </div><!-- / #login-accordion .panel-group -->


                </div><!-- / .col-md-6 -->
              </div><!-- / .row -->
            </div><!-- / .container -->

          </div><!-- / .bg-overlay -->
        </div><!-- / .bg-login -->



     
        <!-- ========== Scripts ========== -->

        <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="assets/js/vendor/google-fonts.js"></script>
        <script src="assets/js/vendor/jquery.easing.js"></script>
        <script src="assets/js/vendor/jquery.waypoints.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/vendor/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/vendor/smoothscroll.js"></script>
        <script src="assets/js/vendor/jquery.localScroll.min.js"></script>
        <script src="assets/js/vendor/jquery.scrollTo.min.js"></script>
        <script src="assets/js/vendor/jquery.stellar.min.js"></script>
        <script src="assets/js/vendor/jquery.parallax.js"></script>
        <script src="assets/js/vendor/slick.min.js"></script>
        <script src="assets/js/vendor/jquery.easypiechart.min.js"></script>
        <script src="assets/js/vendor/countup.min.js"></script>
        <script src="assets/js/vendor/isotope.min.js"></script>
        <script src="assets/js/vendor/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/vendor/wow.min.js"></script>

        <!-- Definity JS -->
        <script src="assets/js/main.js"></script>
    </body>


</html>
