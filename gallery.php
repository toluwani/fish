<!DOCTYPE html>
<html>
	

<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Residency theme">
		<meta name="keywords" content="Residency,sale,buy,rent,pg,house,villa,apartment">
		<meta name="author" content="Fortune Creations">
		<meta name="viewport" content="width=device-width">
		
		<title>FISH- Gallery</title>
		
		<link rel="stylesheet" href="css/font-awesome.min.css">	
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/style.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
	</head>
	<body>
		<header>
			<div class="welcome_header">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<h6>Affortable housing synergy</h6>
						</div>
						<div class="col-sm-6 text-right">
							<ul class="welcome_header_menu">
								<li class="facebook_icon"><a href="#"><i class="fa fa-facebook"> </i></a> </li>
								<li class="twitter_icon"><a href="#"><i class="fa fa-twitter"> </i></a> </li>
								<li class="google_plus_icon"><a href="#"><i class="fa fa-google-plus"> </i></a> </li>
								<li class="pinterest_icon"><a href="#"><i class="fa fa-pinterest"> </i></a> </li>
								<li class="youtube_icon"><a href="#"><i class="fa fa-youtube-play"> </i></a> </li>
							</ul>				
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="menu">
						<nav class="navbar navbar-default">
						  <div class="container-fluid">
						    <!-- Brand and toggle get grouped for better mobile display -->
						    <div class="navbar-header">
						      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						        <span class="sr-only">Toggle navigation</span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						      </button>
						      <a class="navbar-brand" href="index.php">
						      	<img src="images/logo.png" alt="logo">
						      </a>
						    </div>
						
						    <!-- Collect the nav links, forms, and other content for toggling -->
						    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						      <ul class="nav navbar-nav nav_link pull-right">
						        <li><a href="index.php" >HOME </a>
						        	
							    </li>
						      						        
						        <li><a href="gallery.php" >GALLERY </a>
						        	
						        </li>
						  
						        <li><a href="register.php">REGISTER </a> </li>
						      </ul>
						    </div><!-- /.navbar-collapse -->
						  </div><!-- /.container-fluid -->
						</nav>
				</div>
				<div class="contact_info">
				    <div class="contact_detial">
				  		<div class="phone_icon">
				  			<i class="fa fa-phone"> </i>
				  		</div>
				  		<div class="phone_number">
				  			<h5>call us now</h5>
				  			<h2 class="phone_number_h2">215-123-4567</h2>
				  		</div>
				  	</div>    
				</div>
			</div>
   			<div class="inner-page-header-area"> <img alt="banner-image" src="images/innerpage_header.jpg">
				<div class="container">
					<div class="inner_slider_text">
						<div class="property_info_header">
							<h2> Gallery 3 coloumns </h2><h5><a href="index.php">Home/</a> Gallery </h5>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="alert-container">
           <div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
			  <strong>Success!</strong> <span class="alert-message">Better check yourself, you're not looking too good.</span>
			</div>
		</div>	
		<section>
			<div class="inner-page-gallery-three-columns">
				<div class="container">
					<h1 class="header_with_icon">GallerY</h1>
					

					
					<div class=" inner-page-gallery-three-columns-dimension-detail show-hide-detail" id="">
						<ul class="row">
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_1_large.jpg" alt="gallry_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_2_large.jpg" alt="gallry_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>							
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_3_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>			
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_4_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_5_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_6_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>			
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_7_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_8_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>
							<li class="col-md-4 col-sm-4 col-xs-12">
								<img src="images/gallary_image_9_large.jpg" alt="gallery_img">
								<div class="image_description text-cente coloumns" data-toggle="modal" data-target="#image_lightbox">
									<div class="icon"><img src="images/gallary_image_hover_icon.png" alt="+"></div>
									<p class="text-center">Living Room</p>
								</div>								
							</li>							
						</ul>
					</div>
					
					</div>																			
				</div>
			</div>
		</section>
		<div class="modal fade" id="image_lightbox" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="image_lightbox_label">Modal title</h4>
					</div>
					<div class="modal-body">
						<img src="images/hall.jpg" alt="propertyimg" class="img-responsive">
						<button type="button" class="previous_image_btn"><span class="glyphicon glyphicon-menu-left"></span></button>
						<button type="button" class="next_image_btn"><span class="glyphicon glyphicon-menu-right"></span></button>
					</div>
				</div>
			</div>
		</div>	
		<div class="modal fade" id="contact_our_agent" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content" id="model-contact-form">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Contact Our Agent</h4>
					</div>
					<div class="modal-body">
						<div class="contact-form model-contact-form" id="contact-form">
							<ul class="agent-info">
								<li>
									<img class="agent-img" src="images/agent-photo.jpg" alt="agent_photo">
									<h5>Johnathan Doe <span>(Remax Certified Agent)</span></h5>
									<p><span class="glyphicon glyphicon-earphone"></span> 604-786-4440 &nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-envelope"></span> johnathan@gmail.com</p>
								</li>
								<li>
									<img src="images/agent-logo.jpg" alt="agent_logo">
								</li>									
							</ul>
							<form class="agnet-contact-form" name="contact_form" method="post" action="http://dreamvilla.fortune-creations.com/dreamvilla-multiple/functions.php">
								<ul>
									<li>
										<input type="text" name="full_name" id="full_name" placeholder="Full Name" required>
										<input type="number" name="phone_number" id="phone_number" placeholder="Phone Number" required>
										<input type="email" name="email_address" id="email_address" placeholder="Email Address" required>
									</li>
									<li>
										<textarea name="message" id="message" placeholder="Message" required></textarea>
										<input type="submit" name="sendmessage" class="send-message" value="SUBMIT NOW" />
									</li>
								</ul>
							</form>
						</div>
						<p>&nbsp;</p>						
					</div>
				</div>
			</div>
		</div>
		<footer>
			<div class="footer">
				<span class="footer_copyright_text">Copyright 2015.All Rights Reserved by DreamVilla - </span><span class="footer_theme_title">Multi Property Theme</span>
			</div> 
		</footer>	
		<script src="js/jquery-2.1.4.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/image-light-box.js"></script>
		<script src="js/tab-navigation.js"></script>
		<script src="js/formsubmit.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				function centerModals(){
				  $('.modal').each(function(i){
				    var $clone = $(this).clone().css('display', 'block').appendTo('body');
				    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
				    top = top > 0 ? top : 0;
				    $clone.remove();
				    $(this).find('.modal-content').css("margin-top", top);
				  });
				}
				$('.modal').on('show.bs.modal', centerModals);
				$(window).on('resize', centerModals);
			});
		</script>		
	</body>


</html>